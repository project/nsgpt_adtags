<?php
/**
 * @file
 * Admin config for nsgpt_adtags.
 */

/**
 * Hook_admin_form().
 *
 * @param array $form
 *    Form array.
 *
 * @return mixed
 *    return system_settings_form.
 */
function nsgpt_adtags_admin_form($form) {

  $form['nsgpt_adtags'] = array(
    '#type' => 'fieldset',
    '#title' => t('NSGPT tags configuration inside your adserver'),
    '#description' => t('Ad-Serving NSGPT from inside your adserver (DFP)'),
  );
  $form['nsgpt_adtags']['nsgpt_adtags_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Url'),
    '#default_value' => variable_get('nsgpt_adtags_url', ''),
    '#description' => t('The url for the end point'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}
